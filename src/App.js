import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
       <div className="container-fluid">
        <div className="row">
            <div className="floating-box py-5">
                <div className="row">
                    <div className="col-md-6 offset-md-1 py-5 bg-white position-relative">
                        <div className="play-btn">
                            <div className="position-relative h-100 w-100">
                                <img src="img/play.png"  className="play-icon" alt="play"/>
                            </div>
                        </div>
                        <h1 className="text-black lh-2 pr-3">
                            we are think about your future
                        </h1>
                        <p className="color-grey pr-3">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam
                            accusamus, suscipit tempora neque nisi similique nobis quod
                            maxime perferendis provident consectetur sed cupiditate,
                            rerum, dolore dolorem odio reprehenderit ut iusto.
                        </p>
                        <a href="">
                            <div className="contact-btn  middle text-center">
                                <strong>
                                    contact us
                                </strong>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div className="col-md-6 bg-white">
            </div>
            <div className="col-md-6 background-img">
            </div>
        </div>
    </div>
    </div>
  );
}

export default App;
